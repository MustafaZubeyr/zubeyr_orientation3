﻿using UnityEngine.Events;

public static class EventManager 
{

  public static UnityEvent OnGameStart = new UnityEvent();
  public static UnityEvent OnGameEnd = new UnityEvent();
  public static UnityEvent OnLevelStart = new UnityEvent();
  public static UnityEvent OnLevelEnd = new UnityEvent();
  public static UnityEvent OnPass = new UnityEvent();

 

}