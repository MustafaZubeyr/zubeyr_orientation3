﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    
    private Animator Animator;
    public Animator animator
    {
        get
        {
            if(Animator == null)
            Animator=GetComponent<Animator>();

            return Animator;
        }
    }
    
    private void OnEnable()
    {
        EventManager.OnGameStart.AddListener(()=> animator.SetBool("IsGameStarted" ,true));  
    }

    private void OnDisable() 
    {
        EventManager.OnGameStart.RemoveListener(()=> animator.SetBool("IsGameStarted" ,true));
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
