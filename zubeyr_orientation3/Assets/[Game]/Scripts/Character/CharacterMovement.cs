using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;

    public float PlayerSpeed;
    private Vector3 moveDirection;
    private CharacterController controller;
    
    Rigidbody Rigidbody;
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        /*transform.position+=transform.forward.normalized*PlayerSpeed*Time.deltaTime;
        
        if(Input.GetMouseButton(0))
        {
            transform.position -=transform.right*PlayerSpeed*Time.deltaTime;
            
        }

        if(Input.GetMouseButton(1))
        transform.position += transform.right*PlayerSpeed*Time.deltaTime;*/
        Move();
    }

    private void Move()
    {
        float MoveZ = Input.GetAxis("Vertical");
        moveDirection = new Vector3( 0 , 0 , MoveZ);
        moveDirection*= walkSpeed;
        controller.Move(moveDirection * Time.deltaTime);
    }
}
